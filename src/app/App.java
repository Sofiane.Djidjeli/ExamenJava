package app;

import model.Drug;
import model.DrugType;
import views.DrugManagement;
import views.DrugSelect;
import views.Login;

import java.sql.Connection;
import java.sql.DriverManager;

public class App { 
    public static Connection cnx = null;
    public static String connectedUser = null;
    public static String connectedUserRole = null;

    public static void CnxSurGSB() { //Connexion à la BDD
        try {
            cnx = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3306/GSB_Praticien?user=gsbprauser&password=123+aze");         
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
        
    
    public static void main(String[] args) {
        CnxSurGSB();

        new Login();
        if (connectedUser == null) {
            System.exit(1);
        }
        
        run();
    }

    public static void run() {
        DrugSelect drugSelect = new DrugSelect();
        Drug selectedDrug = drugSelect.getSelectedDrug();
        new DrugManagement(selectedDrug);
    }
}

