package dao;

import model.Drug;
import model.DrugReview;
import model.DrugType;
import utils.Const;
import utils.Utils;

import javax.swing.*;

import app.App;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DAODrug { // Dao pour médicament
    public static ComboBoxModel<DrugType> getDrugTypes() throws SQLException {
        List<DrugType> drugTypes = new ArrayList<>();
        
        String query="Select * FROM famille";
    		
        PreparedStatement ps = App.cnx.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
            drugTypes.add(
        		new DrugType(
           		    rs.getString("famcode"),
           		    rs.getString("nom")
                )
            );

		}

        return Utils.listToComboBoxModel(drugTypes);
    }

    public static ComboBoxModel<Drug> getDrugsByType(DrugType drugType) throws SQLException {
        List<Drug> drugs = new ArrayList<>();
      
        String query="Select * FROM medicament WHERE famille=?";
		
        PreparedStatement ps = App.cnx.prepareStatement(query);
        ps.setString(1, drugType.getId());
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
            drugs.add(
        		new Drug(
           		    rs.getString("code"),
           		    rs.getString("nom"),
           		    drugType,
           		    rs.getString("composition"),
           		    rs.getString("effets")
                )
            );
		}
  
        return Utils.listToComboBoxModel(drugs);
    }
    
    public static Object[][] getReviewsForDrug(Drug drug) throws SQLException {
        int numberOfReviewsForDrug = 0;

        String query="Select * FROM observation WHERE mediCode=?";
		
        PreparedStatement ps = App.cnx.prepareStatement(query);
        ps.setString(1, drug.getId());
		ResultSet rs = ps.executeQuery();
		
        numberOfReviewsForDrug = Utils.getResultSetRowCount(rs);
        Object[][] drugReviews = new Object[numberOfReviewsForDrug][2];

        int row=0;
		while (rs.next()) {
			drugReviews[row][0]=Const.DATE_FORMAT.format(rs.getDate("date"));
			drugReviews[row][1]=rs.getString("remarque");
			
			row++;
		}

        return drugReviews;
    }

	public static void addReviewToDrug(String review, String drugId) throws SQLException { // Ajouter medicament
        String query="INSERT INTO `observation` (`date`, `mediCode`,`email`,`remarque`) VALUES (?,?,?,?) ";
		
        PreparedStatement ps = App.cnx.prepareStatement(query);
        ps.setDate(1, new java.sql.Date((new Date()).getTime()));
        ps.setString(2, drugId);
        ps.setString(3, App.connectedUser);
        ps.setString(4, review);

        ps.executeUpdate();
	}
}
