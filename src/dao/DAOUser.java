package dao;

import app.App;
import model.Drug;
import model.Traitement;
import utils.Const;
import utils.Utils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOUser { //Dao pour Utilisateur 
    
    public static String getRole (String email , String mdp) throws SQLException {
        String SQL = "select * from utilisateur where email=? and mot_de_passe=?";
        PreparedStatement ps = App.cnx.prepareStatement(SQL);
        ps.setString(1,email);
        ps.setString(2,Utils.HashPassword(mdp));
        ResultSet rs = ps.executeQuery();
        
        if (rs.next()) {
        return rs.getString("role");
        }
        else {
            return null;

        }
            
    }
    public static Object[][] getReviewsForTraitement(Traitement traitement) throws SQLException {
        int numberOfReviewsForTraitement = 0;

        String query="Select * FROM Traitement WHERE medicament=?";
		
        PreparedStatement ps = App.cnx.prepareStatement(query);
        ps.setString(1, traitement.getId());
		ResultSet rs = ps.executeQuery();
		
        numberOfReviewsForTraitement = Utils.getResultSetRowCount(rs);
        Object[][] Traitement = new Object[numberOfReviewsForTraitement][2];

        int row=0;
		while (rs.next()) {
			Traitement[row][0]=rs.getString("name");
			Traitement[row][1]=rs.getString("medicament");
			
			row++;
		}

        return Traitement;
}
}

    
    
    
    
    
    
    
    
