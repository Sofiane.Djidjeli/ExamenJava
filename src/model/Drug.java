package model;

public class Drug {
    private String id;
    private String description;
    private DrugType type;
    private String composition;
    private String effets;

    public Drug(String id, String description, DrugType type, String composition, String effets) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.composition = composition;
        this.effets = effets;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public DrugType getType() {
        return type;
    }

    public String getComposition() {
        return composition;
    }

    public String getEffets() {
        return effets;
    }

    @Override
    public String toString() {
        return description + " (" + id + ")";
    }
}
