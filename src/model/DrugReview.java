package model;

import java.text.SimpleDateFormat;
import java.util.Date;

import dao.DAODrug;
import utils.Const;

public class DrugReview {
    private String id;
    private String text;
    private Drug drug;
    private long date; // POSIX date format.

    public DrugReview(String id, String text, Drug drug, long date) {
        this.id = id;
        this.text = text;
        this.drug = drug;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Drug getDrug() {
        return drug;
    }

    public long getDate() {
        return date;
    }

    public String getDateAsString() {
        return Const.DATE_FORMAT.format(new Date(date));
    }

    @Override
    public String toString() {
        return text + " (" + id + ") - " + date;
    }
}
