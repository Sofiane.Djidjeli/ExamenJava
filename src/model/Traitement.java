package model;

public class Traitement {
    private String id;
    private String name;
    private DrugType medicament;
    

    public Traitement(String id, String name, DrugType medicament) {
        this.id = id;
        this.name = name;
        this.medicament = medicament;
       
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public DrugType getMedicament() {
        return medicament;
    }
}
