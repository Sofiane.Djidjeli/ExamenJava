package utils;

import java.awt.*;
import java.text.SimpleDateFormat;

public class Const { 
    public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize(); // dimension de l'écran
    public static boolean USE_HEADER_IMAGE_ON_WINDOWS = true; // afficher l'image 
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy"); // Le format de date par défaut dans l'application
}
