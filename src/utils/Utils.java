package utils;

import model.DrugType;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Utils {
    private static <T> T[] listToTypedArray(List<T> list) { // Transforme une liste d'objet de type T en un tableau d'objet de type T
        T[] result = ((T[]) (new Object[list.size()]));
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static <T> ComboBoxModel<T> listToComboBoxModel(List<T> list) { // Construit et retourne un model de donné pour comboBox à partir d'une liste d'objet de Type T
        return new DefaultComboBoxModel<T>(Utils.listToTypedArray(list));
    }



    public static <T> T getComboBoxSelectedItem(ActionEvent event, Class<T> itemType) { // Lors de la selection d'un élement dans une comboBox à partid de l'objet événement, retourne l'élement selectionné
        if (! (event.getSource() instanceof JComboBox)) {
            return null;
        }

        Object selectedItem = ((JComboBox) (event.getSource())).getSelectedItem();
        if (selectedItem == null) {
            return null;
        }

        return ((T) selectedItem);
    }

    public static <T> T getComboBoxSelectedItem(JComboBox comboBox, Class<T> itemType) { // Lors de la selection d'un élement dans une comboBox à partid de l'objet comboBox, retourne l'élement selectionné
        if (comboBox == null) {
            return null;
        }

        Object selectedItem = comboBox.getSelectedItem();
        if (selectedItem == null) {
            return null;
        }

        return ((T) selectedItem);
    }
    
    

    public static void showError(String title, String message) { // Affiche un message d'erreur dans une popup de type Erreur
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }
    
   
    
    public static int getResultSetRowCount(ResultSet resultSet) { // Retourne le nombre de ligne retourner par une requête BDD
        if (resultSet == null) {
            return 0;
        }

        try {
            resultSet.last();
            return resultSet.getRow();
        } catch (SQLException exp) {
            return 0;
        } finally {
            try {
                resultSet.beforeFirst();
            } catch (SQLException exp) {
                return 0;
            }
        }
    }
    public static String HashPassword(String passwordToHash) {
    	
    	MessageDigest md = null;
    	try {
    		md = MessageDigest.getInstance("SHA-1");
    	} catch (NoSuchAlgorithmException e) {
    		e.printStackTrace();
    		return "";
    	}
    	byte[] digest = md.digest(passwordToHash.getBytes());
    	StringBuilder sb = new StringBuilder();
    	for (int i=0; i < digest.length; i++) {
    		sb.append(Integer.toString((digest[i] & 0xff)+ 0x100, 16).substring(1));
    	}
    	return sb.toString();
    }
}
