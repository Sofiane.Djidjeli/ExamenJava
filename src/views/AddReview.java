package views;

import model.Drug;
import utils.Utils;

import javax.imageio.ImageIO;
import javax.swing.*;

import dao.DAODrug;

import static utils.Const.SCREEN;
import static utils.Const.USE_HEADER_IMAGE_ON_WINDOWS;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;

public class AddReview extends JDialog {
    private JTextArea reviewTA = null;

    private Drug drug = null;

    public AddReview(Drug drug) {
        this.drug = drug;

        this.setSize(new Dimension(500, 260));
        this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        this.setResizable(false);
        this.setModal(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JSplitPane pnl0;
        JPanel pnl;
        JLabel lbl;
        JTextField txt;
        JTable tbl;
        JScrollPane sp;
        JButton btn;

        pnl0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pnl0.setDividerSize(0);
        this.add(pnl0, BorderLayout.CENTER);

        if (USE_HEADER_IMAGE_ON_WINDOWS) {
            BufferedImage img = null;
            try {img = ImageIO.read(this.getClass().getClassLoader().getResource("img/logo-with-text.png"));} catch (IOException ex) {ex.printStackTrace();}
            if (img != null) {
                pnl = new ImagePanel(img);
                pnl0.add(pnl);
                this.setSize(new Dimension(
                    Math.max(
                        ((int) (this.getSize().getWidth())),
                        pnl.getWidth()
                    ),
                    ((int) (this.getSize().getHeight())) + pnl.getHeight())
                );
                pnl0.setDividerLocation(pnl.getHeight());
            }

            this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        }

        pnl = new JPanel(null);
        pnl0.add(pnl);

        lbl = new JLabel("Médicament:");
        lbl.setSize(200, 20);
        lbl.setLocation(3, 3);
        pnl.add(lbl);

        txt = new JTextField(drug.toString());
        txt.setSize(this.getWidth() - lbl.getWidth() - 3, 20);
        txt.setLocation(lbl.getWidth(), 3);
        txt.setEditable(false);
        txt.setFont(txt.getFont().deriveFont(Font.BOLD));
        pnl.add(txt);

        reviewTA = new JTextArea();
        reviewTA.setLineWrap(true);
        reviewTA.setText("");
        reviewTA.setEditable(true);
        reviewTA.setFont(reviewTA.getFont().deriveFont(Font.BOLD));
        sp = new JScrollPane(reviewTA, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        sp.setSize(this.getWidth() - 20, 150);
        sp.setLocation(10, 40);
        pnl.add(sp);

        btn = new JButton("Annuler");
        btn.setSize(100, 25);
        btn.setLocation((this.getWidth() - 200) / 3, 203);
        btn.addActionListener(event -> {
            dispose();
        });
        pnl.add(btn);

        btn = new JButton("Valider");
        btn.setSize(100, 25);
        btn.setLocation((((this.getWidth() - 200) / 3) * 2) + 100, 203);
        btn.addActionListener(event -> {
            try {
				DAODrug.addReviewToDrug(reviewTA.getText(), drug.getId());
			} catch (SQLException e) {
	             Utils.showError("Erreur", ((e.getMessage() != null) ? e.getMessage() : "Erreur lors de l'accès à la base de données"));
                 return;			
            }

            dispose();
        });
        pnl.add(btn);

        setVisible(true);
    }
}
