package views;

import app.App;
import dao.DAODrug;
import model.Drug;
import model.DrugType;
import utils.Utils;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

import static utils.Const.SCREEN;
import static utils.Const.USE_HEADER_IMAGE_ON_WINDOWS;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class DrugManagement extends JDialog {
    private JComboBox drugsCB = null;

    private Drug drug = null;
    private Traitement traitement = null;

    public DrugManagement(Drug drug) {
        this.drug = drug;

        this.setSize(new Dimension(500, 260));
        this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        this.setResizable(false);
        this.setModal(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JSplitPane pnl0;
        JPanel pnl;
        JLabel lbl;
        JTextField txt;
        JTextArea mmo;
        JComboBox cbx;
        JButton btn;

        pnl0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pnl0.setDividerSize(0);
        this.add(pnl0, BorderLayout.CENTER);

        if (USE_HEADER_IMAGE_ON_WINDOWS) {
            BufferedImage img = null;
            try {img = ImageIO.read(this.getClass().getClassLoader().getResource("img/logo-with-text.png"));} catch (IOException ex) {ex.printStackTrace();}
            if (img != null) {
                pnl = new ImagePanel(img);
                pnl0.add(pnl);
                this.setSize(new Dimension(
                    Math.max(
                        ((int) (this.getSize().getWidth())),
                        pnl.getWidth()
                    ),
                    ((int) (this.getSize().getHeight())) + pnl.getHeight())
                );
                pnl0.setDividerLocation(pnl.getHeight());
            }

            this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        }

        pnl = new JPanel(null);
        pnl0.add(pnl);

        lbl = new JLabel("Nom:");
        lbl.setSize(200, 20);
        lbl.setLocation(3, 3);
//lbl.setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
        pnl.add(lbl);

        txt = new JTextField(drug.toString());
        txt.setSize(this.getWidth() - lbl.getWidth() - 3, 20);
        txt.setLocation(lbl.getWidth(), 3);
txt.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        txt.setEditable(false);
        txt.setFont(txt.getFont().deriveFont(Font.BOLD));
        pnl.add(txt);

        lbl = new JLabel("Composition:");
        lbl.setSize(200, 20);
        lbl.setLocation(3, 43);
//lbl.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        pnl.add(lbl);

        txt = new JTextField(drug.getComposition());
        txt.setSize(this.getWidth() - lbl.getWidth() - 3, 20);
        txt.setLocation(lbl.getWidth(), 43);
txt.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        txt.setEditable(false);
        txt.setFont(txt.getFont().deriveFont(Font.BOLD));
        pnl.add(txt);

        lbl = new JLabel("Effets:");
        lbl.setSize(200, 20);
        lbl.setLocation(3, 83);
//lbl.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        pnl.add(lbl);

        mmo = new JTextArea();
        mmo.setSize(this.getWidth() - lbl.getWidth() - 3, 100);
        mmo.setLocation(lbl.getWidth(), 83);
mmo.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        mmo.setLineWrap(true);
        mmo.setText(drug.getEffets());
        mmo.setEditable(false);
        mmo.setFont(mmo.getFont().deriveFont(Font.BOLD));
        mmo.setBackground(txt.getBackground());
        pnl.add(mmo);

        int btnsSpace = (this.getWidth() - (100 + 120 + 200 + 100)) / 5;
        btn = new JButton("Quitter");
        btn.setSize(100, 25);
        btn.setLocation(btnsSpace, 203);
        btn.addActionListener(event -> {
            dispose();
        });
        pnl.add(btn);

        btn = new JButton("Voir les avis");
        btn.setSize(120, 25);
        btn.setLocation(btnsSpace * 2 + 100, 203);
        btn.addActionListener(event -> {
            new SeeReviews(drug);
        });
        pnl.add(btn);
       
        if (!App.connectedUserRole.equals("VIS")) {
        	 btn = new JButton("Ajouter un nouvel avis");
             btn.setSize(200, 25);
             btn.setLocation(btnsSpace * 3 + 100 + 120, 203);
             btn.addActionListener(event -> {
                 new AddReview(drug);
             });
             pnl.add(btn);
        }
        pnl.add(btn);
        
        if (!App.connectedUserRole.equals("VIS")) {
        	 btn = new JButton("Voir la synthèse des Traitement");
             btn.setSize(200, 25);
             btn.setLocation(btnsSpace * 4 + 100 + 120 + 200, 203);
             btn.addActionListener(event -> {
                 new Traitement(drug);
             });
             pnl.add(btn);
        }
      
        
        setVisible(true);
    }
}
