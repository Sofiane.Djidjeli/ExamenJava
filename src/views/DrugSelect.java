package views;

import model.Drug;
import model.DrugType;
import utils.Utils;

import javax.imageio.ImageIO;
import javax.swing.*;

import app.App;
import dao.DAODrug;

import static utils.Const.SCREEN;
import static utils.Const.USE_HEADER_IMAGE_ON_WINDOWS;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DrugSelect extends JDialog {
    private JComboBox drugsCB = null;

    private Drug selectedDrug = null;

    public Drug getSelectedDrug() {
        return selectedDrug;
    }

    public DrugSelect() {
        this.setSize(new Dimension(500, 105));
        this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        this.setResizable(false);
        this.setModal(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JSplitPane pnl0;
        JPanel pnl;
        JLabel lbl;
        JComboBox cbx;
        JButton btn;

        pnl0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pnl0.setDividerSize(0);
        this.add(pnl0, BorderLayout.CENTER);

        if (USE_HEADER_IMAGE_ON_WINDOWS) {
            BufferedImage img = null;
            try {img = ImageIO.read(this.getClass().getClassLoader().getResource("img/logo-with-text.png"));} catch (IOException ex) {ex.printStackTrace();}
            if (img != null) {
                pnl = new ImagePanel(img);
                pnl0.add(pnl);
                this.setSize(new Dimension(
                    Math.max(
                        ((int) (this.getSize().getWidth())),
                        pnl.getWidth()
                    ),
                    ((int) (this.getSize().getHeight())) + pnl.getHeight())
                );
                pnl0.setDividerLocation(pnl.getHeight());
            }

            this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        }

        pnl = new JPanel(new GridLayout(0, 2));
        pnl0.add(pnl);

        lbl = new JLabel("Choisir une famille de médicament:");
        pnl.add(lbl);

        cbx = new JComboBox(new DrugType[] {});
        try {
            cbx.setModel(DAODrug.getDrugTypes());
        } catch (SQLException e) {
            Utils.showError("Erreur", ((e.getMessage() != null) ? e.getMessage() : "Erreur lors de l'accès à la base de données"));
            dispose();
            return ;
        }
        cbx.setSelectedIndex(-1);
        cbx.addActionListener(event -> {
            DrugType selectedDrugType = Utils.getComboBoxSelectedItem(event, DrugType.class);
            try {
                drugsCB.setModel(DAODrug.getDrugsByType(selectedDrugType));
            } catch (SQLException e) {
                Utils.showError("Erreur", ((e.getMessage() != null) ? e.getMessage() : "Erreur lors de l'accès à la base de données"));
                dispose();
                return ;
            }
            drugsCB.setSelectedIndex(-1);
        });
        pnl.add(cbx);

        lbl = new JLabel("Choisir un médicament:");
        pnl.add(lbl);

        drugsCB = new JComboBox(new Drug[] {});
        pnl.add(drugsCB);

        btn = new JButton("Quitter");
        btn.addActionListener(event -> {
JOptionPane.showMessageDialog(null, "Bye !"); 
            dispose();
        });
        pnl.add(btn);

        btn = new JButton("Valider");
        btn.addActionListener(event -> {
            if (drugsCB.getSelectedItem() == null) {
                JOptionPane.showMessageDialog(null, "Vous devez choisir un médicament", "Sélection invalide", JOptionPane.ERROR_MESSAGE); //:*:x: TODO: NEXT WINDOW HERE.
                return;
            }
            selectedDrug = Utils.getComboBoxSelectedItem(drugsCB, Drug.class);
//JOptionPane.showMessageDialog(null, "Vous avez choisi " + selectedDrug + "\nNext..."); //:*:x: TODO: NEXT WINDOW HERE.

            dispose();
        });
        pnl.add(btn);

        setVisible(true);
    }

}
