package views;

import static utils.Const.SCREEN;

import java.awt.*;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import app.App;
import dao.DAOUser;

public class Login extends JDialog implements ActionListener {
    JPanel panel;
    JLabel user_label, password_label, message;
    JTextField userName_text;
    JPasswordField password_text;
    JButton submit, cancel;

    public Login() {
        this.setSize(new Dimension(300, 100));
        this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        this.setResizable(false);
        this.setModal(true);

        // User Label
        user_label = new JLabel();
        user_label.setText("Nom:");
        userName_text = new JTextField();
        
        // Password
        password_label = new JLabel();
        password_label.setText("Mot de passe :");
        password_text = new JPasswordField();

        // Submit
        submit = new JButton("OK");

        panel = new JPanel(new GridLayout(3, 1));

        panel.add(user_label);
        panel.add(userName_text);
        panel.add(password_label);
        panel.add(password_text);

        message = new JLabel();
        panel.add(message);
        panel.add(submit);
                
        // Adding the listeners to components..
        submit.addActionListener(this);
        add(panel, BorderLayout.CENTER);
        setTitle("Veuillez-vous connecter !");
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        App.connectedUser=null;
        String email = userName_text.getText();
        String password = password_text.getText();
        
        // Check & getRole
        try {
            App.connectedUserRole= DAOUser.getRole(email,password);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (App.connectedUserRole == null) {
            message.setText(" email ou mot de passe invalide ");
            return;
        }
        
        App.connectedUser=email;
        dispose();
    }
}
