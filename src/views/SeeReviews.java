package views;

import model.Drug;
import utils.Utils;

import javax.imageio.ImageIO;
import javax.swing.*;

import dao.DAODrug;

import static utils.Const.SCREEN;
import static utils.Const.USE_HEADER_IMAGE_ON_WINDOWS;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;

public class SeeReviews extends JDialog {
    private Drug drug = null;

    public SeeReviews(Drug drug) {
        this.drug = drug;

        this.setSize(new Dimension(500, 260));
        this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        this.setResizable(false);
        this.setModal(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JSplitPane pnl0;
        JPanel pnl;
        JLabel lbl;
        JTextField txt;
        JTable tbl;
        JScrollPane sp;
        JButton btn;

        pnl0 = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        pnl0.setDividerSize(0);
        this.add(pnl0, BorderLayout.CENTER);

        if (USE_HEADER_IMAGE_ON_WINDOWS) {
            BufferedImage img = null;
            try {img = ImageIO.read(this.getClass().getClassLoader().getResource("img/logo-with-text.png"));} catch (IOException ex) {ex.printStackTrace();}
            if (img != null) {
                pnl = new ImagePanel(img);
                pnl0.add(pnl);
                this.setSize(new Dimension(
                    Math.max(
                        ((int) (this.getSize().getWidth())),
                        pnl.getWidth()
                    ),
                    ((int) (this.getSize().getHeight())) + pnl.getHeight())
                );
                pnl0.setDividerLocation(pnl.getHeight());
            }

            this.setLocation((SCREEN.width - this.getSize().width) / 2,(SCREEN.height - this.getSize().height) / 2);
        }

        pnl = new JPanel(null);
        pnl0.add(pnl);

        lbl = new JLabel("Observations sur:");
        lbl.setSize(200, 20);
        lbl.setLocation(3, 10);
        pnl.add(lbl);

        txt = new JTextField(drug.toString());
        txt.setSize(this.getWidth() - lbl.getWidth() - 3, 20);
        txt.setLocation(lbl.getWidth(), 10);
        txt.setEditable(false);
        txt.setFont(txt.getFont().deriveFont(Font.BOLD));
        pnl.add(txt);

        String[] columnNames = {"Date", "Observation"};
        try {
	         tbl = new JTable(DAODrug.getReviewsForDrug(drug), columnNames);
        } catch (SQLException e) {
             Utils.showError("Erreur", ((e.getMessage() != null) ? e.getMessage() : "Erreur lors de l'accès à la base de données"));
             dispose();
             return ;
        }
        tbl.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tbl.getColumnModel().getColumn(0).setPreferredWidth(80);
        tbl.getColumnModel().getColumn(1).setPreferredWidth(570);
        tbl.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
        tbl.setFont(txt.getFont().deriveFont(Font.BOLD));
        tbl.setEnabled(false); // Non-éditable.
        tbl.setBackground(new Color(230, 230, 230));
        sp = new JScrollPane(tbl);
        sp.setSize(this.getWidth() - 10, 150);
        sp.setLocation(5, 40);
        tbl.setFillsViewportHeight(true);
        pnl.add(sp);

        btn = new JButton("Retour");
        btn.setSize(100, 25);
        btn.setLocation((this.getWidth() - 100) / 2, 203);
        btn.addActionListener(event -> {
            dispose();
        });
        pnl.add(btn);

        setVisible(true);
    }
}
